<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\models\User;
class LoginTest extends TestCase
{
    use WithFaker;
    /**
     * A basic test example.
     *
     * @return void
     */

    /*
        test to login without email and password
        expected return 422 with error Unauthorized
    */
    public function testLoginWithoutEmailPass()
    {
        $response = $this->json('POST','/api/login');
        $response->assertStatus(422);
        $response->assertJson([ 'message' => 'Invalid data send']);
    }

    // /*
    //     test to login without password
    //     expected return 422 with error Unauthorized
    // */
    public function testLoginWithoutPass()
    {
        $payload = ['email' => 'john@example.com'];
        $response = $this->json('POST','/api/login', $payload);
        $response->assertStatus(422);
        $response->assertJson([ 'message' => 'Invalid data send']);
    }

    /*
        test to login with email and password
        expected return 200 
    */
    public function testLoginWithEmailPass()
    {

        $user = factory(User::class)->create([
            'name' => $this->faker->name,
            'email' =>$this->faker->unique()->safeEmail,
            'password' => \Hash::make('123456'),
        ]);
        $payload = ['email' => $user->email, 'password' => '123456'];

        $this->json('POST', '/api/login', $payload)
            ->assertStatus(200)
            ->assertJsonStructure([
                    'status',
                    'token',
            ]);
    }
    public function testLoginWithInvalidDetails()
    {

        $payload = ['email' => 'doe@test.com', 'password' => '123456'];

        $this->json('POST', '/api/login', $payload)
            ->assertStatus(401)
            ->assertJson([
                'error' => 'Invalid login details'
            ]);
    }
}