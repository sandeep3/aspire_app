<?php

namespace Tests\Unit;

// use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
class RegisterTest extends TestCase
{
    use WithFaker;
    /**
     * A basic unit test example.
     *
     * @return void
     */
     /*
        test to register without email and password and anme
        expected return 422 with error Unauthorized
    */
    public function testRegisterWithoutEmailPassName()
    {
        $response = $this->json('POST','/api/register');
        $response->assertStatus(422);
        $response->assertJson([
            "message" => "Invalid data send",
                "errors" => [
                    "name" => ["The name field is required."],
                    "email" => ["The email field is required."],
                    "password" => ["The password field is required."],
                ]
        ]);
    }
    public function testRegisterWithEmailPassName()
    {
        $regdata = ['email' => $this->faker->unique()->safeEmail,'name'=>$this->faker->name, 'password'=>'password'];
        $response = $this->json('POST','/api/register',$regdata);
        $response->assertStatus(201);
        $response->assertJson([
            "message" => "User create",
        ]);
    }

}
