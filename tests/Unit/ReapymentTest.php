<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\models\User;
use App\models\Loans;

class ReapymentTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_with_out_inputs()
    {
        $user = User::find(1); // sample user
        $token = \JWTAuth::fromUser($user);  
        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
            ->json('POST','/api/loan/repay');
        $response->assertStatus(422);
        $response->assertJson([
            "message" => "Please fill mandatory filelds",
                "errors" => [
                    "loan_id" => ["The loan id field is required."],
                    "amount" => ["The amount field is required."],
                ]
        ]);
    }
    public function test_with_invalid_amount()
    {
        $user = User::find(1); // sample user
        $token = \JWTAuth::fromUser($user);
        $payData =['loan_id'=>2,'amount'=>'wefwe'];
        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
            ->json('POST','/api/loan/repay',$payData);
        $response->assertStatus(422);
        $response->assertJson([
            "message" => "Please fill mandatory filelds",
                "errors" => [
                    "amount" => ["The amount must be a number."],
                ]
        ]);
    }
    public function test_with_valid_amount_approved_loan()
    {
        $loan = Loans::where('status','approved')->first();
        $user = User::find(1); // sample user
        $token = \JWTAuth::fromUser($user);
        $payData =['loan_id'=>$loan->id,'amount'=>432];
        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
            ->json('POST','/api/loan/repay',$payData);
        $response->assertStatus(200);
        $response->assertJson([
            'data'=>[
                'status'=>'success',
                'message'=>'Repayments updated successfully',
            ],
        ]);
    }
    public function test_pending_loan()
    {
        $loan = Loans::where('status','pending')->first();
        $user = User::find(1); // sample user
        $token = \JWTAuth::fromUser($user);
        $payData =['loan_id'=>$loan->id,'amount'=>432];
        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
            ->json('POST','/api/loan/repay',$payData);
        $response->assertStatus(200);
        $response->assertJson([
            'data'=>[
                'status'=>'fails',
                'message'=>'Your loan is not approved.'
            ],
            'validation_status'=>false,
        ]);
    }
    public function test_completed_loan()
    {
        $loan = Loans::where('status','completed')->first();
        $user = User::find(1); // sample user
        $token = \JWTAuth::fromUser($user);
        $payData =['loan_id'=>$loan->id,'amount'=>432];
        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
            ->json('POST','/api/loan/repay',$payData);
        $response->assertStatus(200);
        $response->assertJson([
            'data'=>[
                'status'=>'fails',
                'message'=>'Your loan is completed.'
            ],
            'validation_status'=>false,
        ]);
    }
}
