<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\models\User;
class LoanTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testLoanWithoutDurationAmountIntrestRate()
    {
        $user = User::find(1); // sample user
        $token = \JWTAuth::fromUser($user);  
        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
            ->json('POST','/api/loan/apply');
        $response->assertStatus(422);
        $response->assertJson([
            "message" => "Please fill mandatory filelds",
                "errors" => [
                    "amount" => ["The amount field is required."],
                    "terms" => ["The terms field is required."],
                    "interest_rate" => ["The interest rate field is required."],
                ]
        ]);
    }
    public function testLoanInvalidtDurationAmountIntrestRate()
    {
        $user = User::find(1); // sample user
        $token = \JWTAuth::fromUser($user);
        $loanData=['amount'=>'qwe','terms' =>'asd','interest_rate'=>'qwe'];
        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
            ->json('POST','/api/loan/apply',$loanData);
        $response->assertStatus(422);
        $response->assertJson([
            "message" => "Please fill mandatory filelds",
                "errors" => [
                    "amount" => ["The amount must be a number."],
                    "terms" => ["The terms must be a number."],
                    "interest_rate" => ["The interest rate must be a number."],
                ]
        ]);
    }
    public function testLoanWithoutAuth()
    {
        $loanData=['amount'=>10000,'terms' =>1,'interest_rate'=>9.5];
        $response = $this->json('POST','/api/loan/apply',$loanData);
        $response->assertStatus(401);
        $response->assertJson([
            "status"=> "Authorization Token not found"
        ]);
    }
    public function testLoanWithAuth()
    {
        $user = User::find(1); // sample user
        $token = \JWTAuth::fromUser($user);
        $loanData=['amount'=>10000,'terms' =>1,'interest_rate'=>9.5];
        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
        ->json('POST','/api/loan/apply',$loanData);
        $response->assertStatus(200);
        $response->assertJson([
                    'status'=>'success',
                    'message'=>'Successfully applied loan.'
                ]);
    }
    public function approve_loan_with_auth_with_loan_id(){
        $user = User::find(1); // sample user
        $token = \JWTAuth::fromUser($user);
        $loanData =['id'=>3];
        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
        ->json('POST','/api/loan/approve',$loanData);
        $response->assertStatus(200);
        $response->assertJson([
                    'status' => "approved",
                ]);
    }
}
