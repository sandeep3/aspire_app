<?php

namespace App\Http\Controllers\api\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RepaymentRequest;
use App\Repositories\RepaymentRepository;


class RepaymentController extends Controller
{
    public function postRepay(RepaymentRequest $request,RepaymentRepository $RepaymentRepository){
        $response =$RepaymentRepository->save($request);
        return response()->json($response);
    }   
}
