<?php

namespace App\Http\Controllers\api\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRegister;
use App\Http\Requests\UserLogin;
use App\Repositories\UserRepository;
class AuthController extends Controller
{
    public function postRegister(UserRegister $request,UserRepository $UserRepository){
        $user= $UserRepository->save($request);
        return response()->json(['message'=>'User create'],201);
    }
    public function postLogin(UserLogin $request){
        $credentials = $request->only('email', 'password');
        if ($token = \JWTAuth::attempt($credentials)) {
            return response()->json(['status' => 'success','token'=>'Bearer '.$token], 200)->header('Authorization', $token);
        }
        return response()->json(['error' => 'Invalid login details'], 401);
    }
}
