<?php

namespace App\Http\Controllers\api\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\LoanRepository;
use App\Http\Requests\LoanRequest;
class LoanController extends Controller
{
    public function postApply(LoanRequest $request,LoanRepository $loanRepository){
        $emi_amount=$loanRepository->save($request);
        return response()->json(['status'=>'success','message'=>'Successfully applied loan.']);
    }
    public function getLoans(LoanRepository $loanRepository){
        $lons=$loanRepository->getAll();
        return response()->json(['status'=>'success','data'=>$lons]);
    }

    public function postApprove(Request $request,LoanRepository $loanRepository){
        $response=$loanRepository->approve($request);
        return response()->json(['status'=>$response]);
    }
         
}
        
