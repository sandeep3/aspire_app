<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Loans extends Model
{
    protected $table="loans";

    protected $fillable=['user_id','amount','duration','intrest_total','intrest_rate','emi_amount','status'];
    
    protected $casts=['created_at'=>'date'];

    public function user(){
        return $this->hasOne('App\models\User','id','user_id');
    }
    public function rey_payments(){
        return $this->hasMany('App\models\Repayments','loan_id','id');
    }
    public function TotalLoan(){
        $total = ($this->emi_amount * ($this->duration * 12));
        return round($total);
    }
    public function TotalIntrest(){
        $Intrest = $this->TotalLoan() - $this->amount;
        return round($Intrest);
    }
    public function TotalDue(){
        $balance = $this->TotalLoan() - $this->rey_payments()->sum('amount');
        return round($balance);
    }
}
