<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Repayments extends Model
{
    protected $table="repayments";

    protected $fillable=['user_id','loan_id','amount'];

    
}
