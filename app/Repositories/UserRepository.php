<?php 
namespace App\Repositories;
use App\models\User;
class UserRepository {


    public function save($UserData){
        $user = new User;
        $user->email = $UserData->email;
        $user->password = \Hash::make($UserData->password);
        $user->name = $UserData->name;
        $user->save();
        return $user;
    }
}