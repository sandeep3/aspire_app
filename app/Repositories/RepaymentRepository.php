<?php 
namespace App\Repositories;
use App\models\Repayments;
use App\models\Loans;
class RepaymentRepository {


    public function save($paymentData){
        $loan =Loans::find($paymentData->loan_id);
        $validation=  SELF::Validate($paymentData,$loan);
        if($validation['validation_status']==true){
            if($loan->TotalDue()!=0){
                $repayment = new Repayments;
                $repayment->loan_id = $loan->id;
                $repayment->user_id = $loan->user_id;
                $repayment->amount = $paymentData->amount;
                // $repayment->save();

                if($loan->TotalDue()==0){
                   SELF::UpdateLoanStatus($loan,'completed');
                }
                return[
                    'data'=>[
                        'status'=>'success',
                        'message'=>'Repayments updated successfully',
                    ],
                ];
            }else{
                SELF::UpdateLoanStatus($loan,'completed');
                return [
                    'data'=>[
                        'status'=>'fails',
                        'message'=>'Your loan is completed.'
                    ],
                    'validation_status'=>false,
                ];
            }

        }else{
            return $validation;
        }
    }
    public function UpdateLoanStatus($loan,$status){
        $loan->status=$status;
        $loan->save();
    }
    public function Validate($data,$loan){
       
        if(!empty($loan)){

            if($loan->status=='approved' ){
                    return [
                        'validation_status'=>true,
                    ];
            }if($loan->status=='pending'){
                return [
                    'data'=>[
                        'status'=>'fails',
                        'message'=>'Your loan is not approved.'
                    ],
                    'validation_status'=>false,
                ];
            }
            if($loan->status=='completed'){
                return [
                    'data'=>[
                        'status'=>'fails',
                        'message'=>'Your loan is completed.'
                    ],
                    'validation_status'=>false,
                ];
            }

        }
        else{
            return [
                'data'=>[
                    'status'=>'fails',
                    'message'=>'Could not find loan application'
                ],
                'validation_status'=>false,
            ];
        }
    }
}