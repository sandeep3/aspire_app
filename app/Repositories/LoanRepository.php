<?php 
namespace App\Repositories;
use App\models\Loans;
class LoanRepository {

    public function save($loanData){
        $emi = SELF::emi_calculator($loanData->amount, $loanData->interest_rate, $loanData->terms);
        $loan= new Loans;
        $loan->user_id=\Auth::user()->id;
        $loan->amount=$loanData->amount;
        $loan->duration=$loanData->terms;
        $loan->interest_rate=$loanData->interest_rate;
        $loan->emi_amount=$emi;
        $loan->save();
        return $emi;
    }
    public function getAll(){
        return SELF::TansformData(Loans::orderBy('created_at','desc')->get());
    }

    public function approve($ApproveData){
        $loan= Loans::where(['id'=>$ApproveData->id])->first();
        if(!empty($loan) && $loan->status=='pending'){
            $loan->status='approved';
            $loan->save();
            return 'approved';
        }else{
            return 'fails';
        }
    }
    // helpers
    function emi_calculator($p, $r, $t) 
    { 
        $emi; 
        $r = $r / (12 * 100); 
        $t = $t * 12;  
        $total_intrest=$t*$r;
        $emi = ($p * $r * pow(1 + $r, $t)) /  
                    (pow(1 + $r, $t) - 1); 
    
        return ($emi); 
    } 
    public function TansformData($data){
        foreach($data as $key=>$val){
            $resource[]=[
                'id'=>$val->id,
                'user_id'=>$val->user_id,
                'amount'=>$val->amount,
                'interest_rate'=>$val->interest_rate,
                'monthly_emi'=>$val->emi_amount,
                'status'=>$val->status,
                'date'=>$val->created_at,
                'user_data'=>[
                    'name'=>$val->user->name,
                    'email'=>$val->user->email
                ],
            ];
        }
        return $resource;
    }
}