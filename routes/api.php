<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([],function(){

    Route::post('register','api\user\AuthController@postRegister');
    Route::post('login','api\user\AuthController@postLogin');
    Route::group(['middleware'=>'jwt.verify'],function(){
        Route::group(['prefix'=>'loan'],function(){
            Route::post('apply','api\user\LoanController@postApply');
            Route::get('list','api\user\LoanController@getLoans');
            Route::post('approve','api\user\LoanController@postApprove');
            Route::post('repay','api\user\RepaymentController@postRepay');
        });
    });

});
