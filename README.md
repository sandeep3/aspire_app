
## Installation 

Clone the repository
- cd to aspire_app.
- Run composer update.
- Run cp .env.example .env or copy .env.example .env.
- Run php artisan key:generate.
- Run php artisan migrate.
- Run php artisan serve.
- Server will run on: localhost:8000.


## Postman Collections
    - Download the postman collections from https://www.getpostman.com/collections/1b27c226899a077ab3be
    - Documentation URL: https://documenter.getpostman.com/view/4832809/TzeZD66b

## Unit test

    -  To run all unit tests run   ./vendor/bin/phpunit
    -  For individual tests run Ex: ./vendor/bin/phpunit --filter RegisterTest
    
# Choices
    -> To apply for the loan, User should be registered with the app. After registration user should login.
    -> After applying for the loan, default loan status will be pending. To avoid repaymenst from the user.
    -> Once the loan is approved, User should be able to do reypayments.
    -> In Loan model calulating the total loan (with intrest) and Due amount. To check the repayments to save and update the status to completed.